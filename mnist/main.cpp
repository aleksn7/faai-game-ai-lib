#include <regex>
#include <fstream>
#include <iostream>
#include <map>
#include "layers.h"
#include "networks.h"

#define OUT_NEURONS 10
#define HIDDEN_NEURONS 100
#define INPUT_NEURONS 784
#define TRAIN_COUNT 1000
#define TEST_COUNT 100
#define VISUALIZAITION false

std::vector<std::string> split(const std::string& input, const std::string& regex) {
    // passing -1 as the submatch index parameter performs splitting
    std::regex re(regex);
    std::sregex_token_iterator
        first{input.begin(), input.end(), re, -1},
        last;

    return {first, last};
}

void vecPtrinter(const std::vector<std::string>& vec) {
    for (const auto& item : vec) {
        std::cout << item << ", ";
    }
    std::cout << std::endl;
    std::cout << vec.size() << std::endl;
}

void train(const std::string& filepath, faai::Neural& network) {
    std::ifstream fin;
    fin.open(filepath);
    int count = 0;
    while (count < TRAIN_COUNT) {
        count++;
        std::string line;
        std::getline(fin, line);
        if (fin.eof()) {
            break;
        }
        std::vector<std::string> data_vector = split(line, ",");
        if (data_vector.size() != 785) {
            break;
        }
        std::size_t answer = std::atoi(data_vector[0].c_str());
        data_vector.erase(data_vector.begin());
        prep::Matrix data_matrix(data_vector);
        prep::Matrix oo1_matrix(1, data_vector.size(), 0.01);

        data_matrix = (data_matrix * (1.0 / 255.0) * 0.99) + oo1_matrix;

        prep::Matrix target_matrix(1, OUT_NEURONS);
        for (std::size_t i = 0; i < OUT_NEURONS; i++) {
            target_matrix(0, i) = 0.01;
            if (i == answer) {
                target_matrix(0, i) = 0.99;
            }
        }
        network.train(data_matrix.transp(), target_matrix.transp());
    }
    fin.close();
}

void printDigit(const prep::Matrix& data_matrix) {
    for (std::size_t i = 0; i < 28; i++) {
        for (std::size_t j = 0; j < 28; j++) {
            if (data_matrix(0, i * 28 + j) > 0.02) {
                std::cout << "#";
            } else {
                std::cout << "_";
            }
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void query(const std::string& filepath, faai::Neural& network) {
    std::ifstream fin;
    fin.open(filepath);
    int count_true_answer = 0;
    int count_all = 0;
    while (count_all < TEST_COUNT) {
        std::string line;
        std::getline(fin, line);
        if (fin.eof()) {
            break;
        }
        count_all++;
        std::vector<std::string> data_vector = split(line, ",");
        if (data_vector.size() != 785) {
            break;
        }
        std::size_t answer = std::atoi(data_vector[0].c_str());
        data_vector.erase(data_vector.begin());
        prep::Matrix data_matrix(data_vector);
        prep::Matrix oo1_matrix(1, data_vector.size(), 0.01);

        data_matrix = (data_matrix * (1.0 / 255.0) * 0.99) + oo1_matrix;
        prep::Matrix output_array = network.query(data_matrix.transp());

        prep::Matrix target_matrix(1, OUT_NEURONS);
        for (std::size_t i = 0; i < OUT_NEURONS; i++) {
            target_matrix(0, i) = 0.01;
            if (i == answer) {
                target_matrix(0, i) = 0.99;
            }
        }

        float max_arg = -2;
        std::size_t max_index = 0;
        for (std::size_t i = 0; i < OUT_NEURONS; i++) {
            if (max_arg < output_array(i, 0)) {
                max_arg = output_array(i, 0);
                max_index = i;
            }
        }
        if (max_index == answer) {
            count_true_answer++;
        } else {
            if (VISUALIZAITION) {
                std::cout << max_index << std::endl;
                printDigit(data_matrix);
            }
        }
    }
    std::cout << std::endl << "Процент правильных ответов: ";
    std::cout << 100 * static_cast<float>(count_true_answer) / static_cast<float>(count_all);
    std::cout << "%" << std::endl << std::endl;
    fin.close();
}

int main() {
    clock_t start = clock();
    faai::Neural network(784, 0.2);  // Create network
    network.addLayer(faai::Biased(100, faai::activation::sigmoid));
    network.addLayer(faai::Dense(10, faai::activation::sigmoid));  // Add output layer

    train("train.csv", network);
    query("train.csv", network);  // Test network

    network.save("test.nn");

    faai::Neural new_neural("test.nn");  // Create new network from file

    new_neural.setLearningRate(0.1);  // Change learning rate

    train("train.csv", new_neural);
    query("train.csv", new_neural);

    clock_t end = clock();
    float seconds = static_cast<float>(end - start) / CLOCKS_PER_SEC;
    std::cout << seconds << std::endl;
}

