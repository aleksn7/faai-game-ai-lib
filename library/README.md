# Работа с библиотекой faai

## Создание парцептрона

faai::Neural network(start_neurons, learning_rate);
 • start_neurons - кол-во входных нейронов
 • learning_rate - коэффициент обучения
 
## Создание и добавление слоя
 
faai::LAYER_TYPE layer(neurons, faai::acitvation::[function name]);

 • LAYER_TYPE - тип слоя (ниже представленны допустимые типы)
 
 • • Dense - Полносвязанный без нейрона смещения
 
 • • Biast - Полносвязанный с нейроном смещения
 
 • • Recurent - Рекурентный слой
 
 • neurons - кол-во нейронов
 
 • activation - функция активации
 ПРИМЕЧАНИЕ!
 Функции активации находятся в faai::activaion::[function name](ниже представлены допустимые функции)
 
 • • sigmoid - сигмоида
 
 • • th – гиперболический тангенс
 
 • • relu - выпрямитель

## Методы faai::Neural
• • Neural(std::size_t input_nodes, float learning_rate);

• • Neural(std::string filename);

• • void addLayer(TLayer layer);

• • void save(std::string filename);

• • void setLearningRate(float learningrate);

• • prep::Matrix query(const prep::Matrix& input);

• • void train(const prep::Matrix& input, const prep::Matrix& target); 

• • float getWeight(int layer_from, int n_from, int n_to);


## Методы AI::AI
Создает ai с указанными параметрами и нейронной сетью из файла в противном же случае генерируется новая нейроная сеть
• • AI(std::size_t actions_count, 
        std::map<int, float> objects, 
        std::size_t input_count, filename);

 • actions_count - кол-во действий которое может совершить ai
 • objects – представление объектов в виде float
 • input_count – кол-во входных данных
 • filename – имя файла из которого будет извлечена нейроная сеть

Возращает данные, которые вернула нейронная сеть
• • std::vector<float> step(const std::vector<int>& env);

Регистрирует фидбэк среды
• • void registerResult(float result);

Тренирует ai на основе всех данный пропущенных через нейронную сеть до этого момента
• • void trainAll();

Тренирует ai на основе последнего действия и полученного фидбека
• • void trainNow(float result);

Сохраняет нейронную сеть в файл
• • void save(std::string filename);
 