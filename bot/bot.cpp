#include "bot.h"

BOT::BOT(std::string name) : name(name) {}
// AI(std::size_t actions_count, std::map<int, float> objects, std::size_t input_count, std::string filename);

int BOT::step(const std::vector<int>& env) {
    // This implements the logic for the converting current environment to ai's input vector
    // and transforming ai's output to a relevant form indicating an action.
    // std::vector<float> network_output = ai.step(env);
    int actions_count = 9;
    return rand() % (actions_count + 1);
}

void BOT::registerResult(const std::vector<int>& env, float award) {
    // This implements the logic of holding data
    // corresponding to the last ai's action and it's successfulness over
    // to train with it by the "trainAll" method.
    // ai.regitsterResult(env, award);
}

void BOT::trainAll() {
    // This implements the logic of training
    // with all the data stored by the register result method.
    // ai.trainAll();
}

void BOT::trainNow(float award) {
    // This implements the logic of training
    // with data from the last action and the award passed.
    // ai.trainNow(award);
}

void BOT::save() const {
    // This implements the logic of writting all the required model-related data
    // to the file 'name' (both relative and absolute paths are supported).
    // ai.save(name);
}

